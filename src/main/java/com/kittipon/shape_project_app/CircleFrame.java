/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipon.shape_project_app;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author kitti
 */
public class CircleFrame extends JFrame {

    JLabel lblRadius;
    JTextField txtRadius;
    JButton btnCalculate;
    JLabel lblResult;

    public CircleFrame() {
        super("Circle");
        this.setSize(300, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblRadius = new JLabel("radius : ", JLabel.TRAILING);
        lblRadius.setSize(50, 20);
        lblRadius.setLocation(5, 5);
        lblRadius.setBackground(Color.WHITE);
        lblRadius.setOpaque(true);
        this.add(lblRadius);

        txtRadius = new JTextField();
        txtRadius.setSize(50, 20);
        txtRadius.setLocation(60, 5);
        this.add(txtRadius);

        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        this.add(btnCalculate);

        lblResult = new JLabel("Circle radius = ??? area = ??? parameter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(300, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.GRAY);
        lblResult.setOpaque(true);
        this.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    System.out.println("Clicked!!!");
                    //1.Get text form txtRadius -> strRadius
                    String strRadius = txtRadius.getText();
                    // 2.Change strRadius -> radius : double phaseDouble
                    double radius = Double.parseDouble(strRadius); // -> NumberFormatException 
                    //3.Instance object Circle(radius) -> circle 
                    Circle circle = new Circle(radius);
                    //4.Update lblResult data from circle to show
                    lblResult.setText("Circle  = " + String.format("%.2f", circle.getRadius())
                            + " area = " + String.format("%.2f", circle.calArea())
                            + " parimeter = " + String.format("%.2f", circle.calPerimater()));
                } catch (Exception ex) {
                    System.out.println("Error!!!" + ex.getMessage());
                    System.out.println(ex.getCause());
                    JOptionPane.showMessageDialog(CircleFrame.this, "Error : Please input number",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtRadius.setText("");
                    txtRadius.requestFocus();

                }

            }

        });

    }

    public static void main(String[] args) {
        CircleFrame frame = new CircleFrame();
        frame.setVisible(true);

    }
}
